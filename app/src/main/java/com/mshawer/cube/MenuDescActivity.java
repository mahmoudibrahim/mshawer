/**
 * 
 */
package com.mshawer.cube;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;

import com.mshawer.cube.utils.Const;

import javax.xml.transform.TransformerException;

/**
 * @author Kishan H Dhamat
 * 
 */
public class MenuDescActivity extends ActionBarBaseActivitiy {
	private WebView webView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu_desc_activity);
		setIcon(R.drawable.back);
		setIconMenu(R.drawable.taxi);
		setTitle(getIntent().getStringExtra(Const.Params.TITLE));
		webView = (WebView) findViewById(R.id.wvDesc);
		webView.loadData(getIntent().getStringExtra(Const.Params.CONTENT),
				"text/html", "utf-8");
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnActionNotification:
			onBackPressed();
			break;

		default:
			break;
		}
	}

	@Override
	protected boolean isValidate() {
		return false;
	}



	@Override
	public void warning(TransformerException exception) throws TransformerException {

	}

	@Override
	public void error(TransformerException exception) throws TransformerException {

	}

	@Override
	public void fatalError(TransformerException exception) throws TransformerException {

	}
}
