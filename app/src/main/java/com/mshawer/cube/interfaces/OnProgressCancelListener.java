package com.mshawer.cube.interfaces;

/**
 * @author Hardik A Bhalodi
 */
public interface OnProgressCancelListener {
	public void onProgressCancel();

}
